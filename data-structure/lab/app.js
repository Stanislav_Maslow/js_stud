'use strict';

var input = document.getElementById('input');

function showBestCust(data) {
	var str = [];
	var numOfTimes = 0;
	var nameWorstCust;
	var listBestProd = [];

	data.forEach(function(item) {
		str.push(item.split(', ', 2));
	});

	var countCust = _.countBy(str, function(elem) {
		return elem[1];
	});
	// var countCust = {};
	// for (var i = 0; i < str.length; ++i) {
	// 	var elem = str[i][1];
	// 	if (countCust[elem] != undefined)//// ****equivalent of _.countBy**/
	// 		countCust[elem]++;
	// 	else{
	// 		countCust[elem] = 1;
	// 	}
	// }

	for (var key in countCust) {
		if (countCust[key] > numOfTimes) {
			numOfTimes = countCust[key];
			nameWorstCust = key;
		}
	}
	for (var key in countCust) {
		if (countCust[key] === numOfTimes) {
			listBestProd.push(' ' + key +' '+ countCust[key] +' times');
		}
	}
	
	return input.innerHTML = '<p>' + listBestProd + '</p>';
}

function showWorstCust(data) {
	var str = [];
	var numOfTimes = 2500;
	var nameWorstCust;
	var listWorstCust = [];

	data.forEach(function(item) {
		str.push(item.split(', ', 2));
	});

	var countCust = _.countBy(str, function(elem) {

		return elem[1];
	});

	for (var key in countCust) {
		if (countCust[key] < numOfTimes) {
			numOfTimes = countCust[key];
			nameWorstCust = key;
		}
	}
	for (var key in countCust) {
		if (countCust[key] === numOfTimes) {
			listWorstCust.push(' ' + key+' '+ countCust[key] +' times');
		}
	}
	
	return input.innerHTML = '<p>' + listWorstCust + '</p>';
}

function showBestProd(data) {
	var str = [];
	var numOfTimes = 0;
	var nameBestCust;
	var listBestProd = [];

	data.forEach(function(item) {
		str.push(item.split(', '));
	});

	var countCust = _.countBy(str, function(elem) {

		return elem[3];
	});

	for (var key in countCust) {
		if (countCust[key] > numOfTimes) {
			numOfTimes = countCust[key];
			nameBestCust = key;
		}
	}
	for (var key in countCust) {
		if (countCust[key] === numOfTimes) {
			listBestProd.push(' ' + key+' '+ countCust[key] +' times');
		}
	}
	
	return input.innerHTML = '<p>' + listBestProd + '</p>';
}

function showWorstProd(data) {
	var str = [];
	var numOfTimes = 2500;
	var nameWorstProd;
	var listWorstProd = [];

	data.forEach(function(item) {
		str.push(item.split(', '));
	});

	var countCust = _.countBy(str, function(elem) {

		return elem[3];
	});

	for (var key in countCust) {
		if (countCust[key] < numOfTimes) {
			numOfTimes = countCust[key];
			nameWorstProd = key;
		}
	}
	for (var key in countCust) {
		if (countCust[key] === numOfTimes) {
			listWorstProd.push(' ' + key+' '+ countCust[key] +' times');
		}
	}
	
	return input.innerHTML = '<p>' + listWorstProd + '</p>';
}